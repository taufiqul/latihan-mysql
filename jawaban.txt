1. Membuat Database:

   MariaDB [(none)]> create database myshop;
   MariaDB [(none)]> use myshop;

2. Membuat Table di Dalam Database:

    MariaDB [myshop]> create table users(
    -> id int auto_increment,
    -> name varchar(255),
    -> email varchar(255),
    -> password varchar(255),
    -> primary key(id)
    -> );

   MariaDB [myshop]> create table categories(
    -> id int auto_increment,
    -> name varchar(255),
    -> primary key(id)
    -> );

   MariaDB [myshop]> create table items(
    -> id int auto_increment,
    -> name varchar(255),
    -> description varchar(255),
    -> price int,
    -> stock int,
    -> category_id int,
    -> primary key(id),
    -> foreign key (category_id) references categories(id)
    -> );

3. Memasukkan Data pada Table:

   MariaDB [myshop]> insert into users(name, email, password) VALUES ("John Doe", "john@doe.com", "john123");
   MariaDB [myshop]> insert into users(name, email, password) VALUES ("John Doe", "jane@doe.com", "jenita123");

   MariaDB [myshop]> insert into categories(name) VALUES ("gadget"), ("cloth"), ("men"), ("women"), ("branded");

   MariaDB [myshop]> insert into items Values(
    -> null, "Sumsang b50", "hape keren dari merek sumsang", 4000000, 100, 1);
   MariaDB [myshop]>  insert into items Values(
    -> null, "Uniklooh", "baju keren dari brand ternama", 500000, 50, 2), (null, "IMHO Watch", "jam tangan anak yang jujur banget", 2000000, 10, 1);

4. Mengambil Data dari Database:

   a. Mengambil data users expect password: 
      MariaDB [myshop]> select id, name, email from users;
   b. Mengambil data items:
      MariaDB [myshop]> select * from items order by price desc;
   c. Menampilkan data items join dengan kategori:
      MariaDB [myshop]> select * from items WHERE name like "%unik%";

5. Mengubah Data dari Database:

   MariaDB [myshop]> update items set price ="2500000" where id="1";